define([
  'underscore',
  'backbone',
], function(_, Backbone) {
  var AppRouter = Backbone.Router.extend({
    routes: {
      '/test': 'test'
    }
  });

  var initalize = function() {
    var app_router = new AppRouter;
    app_router.on('test', function() {
      console.log('test');
    });
    Backbone.history.start();
  };
  return {
    initialize: initialize
  };
});
