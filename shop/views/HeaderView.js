define([
  'underscore',
  'backbone',
  'templates/Template'
], function(_, Backbone, Template) {
  var HeaderView = Backbone.View.extend({
 
  template: Template['header'],
  
  tagName: "h1",
  className: "brand",

  initialize: function () {
//    _.bindAll(this);
  },

  render: function() {
    this.$el.html(this.template({}));
    return this;
  }

  });
  
  return HeaderView;

});
