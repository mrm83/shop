module.exports = function(grunt) {
grunt.initConfig({
  pkg: grunt.file.readJSON('package.json'),
  requirejs: {
    compile: {
      options: {
        optimize: "uglify",
        baseUrl: "./",
        out: "../public_html/static/js/shop/cowbeans.min.js",
        name: "app",
        keepBuildDir: true,
        paths: {
          'jquery': 'empty:',
          'underscore': 'empty:',
          'json': 'empty:',
          'backbone': 'empty:',
          'handlebars': 'empty:'
        }
      }
    }
  },
  less: {
    all: {
      src: './less/main.less',
      dest: '../public_html/static/css/shop/shop.css'
    }
  }

});

grunt.loadNpmTasks('grunt-contrib-requirejs');
grunt.loadNpmTasks('grunt-contrib-less');
//grunt.loadNpmTasks('grunt-contrib-less');
grunt.registerTask('default', ['requirejs', 'less']);

}
