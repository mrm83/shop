define([
  'underscore',
  'backbone',
  'templates/Template',
  'views/HeaderView',
  'views/PostView'
], function(_, Backbone, Template, HeaderView, PostView) {
  var ShopView = Backbone.View.extend({
 
  initialize: function () {
//    _.bindAll(this);
  },

  render: function() {
    this.drawHeader(); 
    this.drawPosts();
    return this;
  },

  drawHeader: function () {
    var header = new HeaderView();
    header.render();
    this.$el.append(header.el);
  },

  drawPosts: function () {
    var post = new PostView();
    post.render();
    this.$el.append(post.el);
  }

  });
  
  return ShopView;

});
