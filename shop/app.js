require.config({
  paths: {
    "jquery": "http://www.cowbeans.com/static/js/lib/jquery-1.10.2.min",
    "underscore": "http://www.cowbeans.com/static/js/lib/underscore-min",
    "json": "http://www.cowbeans.com/static/js/lib/json2",
    "backbone": "http://www.cowbeans.com/static/js/lib/backbone-min",
    "handlebars": "http://www.cowbeans.com/static/js/lib/handlebars"
  },
  shim: {
    'jquery': {},
    'underscore': {
      exports: "_"
    },
    'backbone': {
      deps: ['underscore', 'jquery'],
      exports: 'Backbone'
    },
    'handlebars': {
      deps: ['backbone', 'jquery'],
      exports: 'Handlebars'
    }
  }
});

require([
  "jquery",
  "views/ShopView"
], function($, ShopView) {
  var ShopView = new ShopView({ el: $('body') });
  ShopView.render();
});
