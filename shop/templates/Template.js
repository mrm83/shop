define(['handlebars'], function(Handlebars) {
var Templates = {};

Templates['cowbeans'] =
  Handlebars.compile('cowbeans');

Templates['header'] =
  Handlebars.compile(
    "COWBEANS"
  );

Templates['post'] =
  Handlebars.compile(
    "<div class='post float-left box-pad-sm relative'>"
    + "<div class='post_title hidden fix-top-left box-pad-sm box-marg bg-white bold'>{{date}} - {{title}}</div>"
    + "<img src='{{imageSrc}}' class='post_image thin-border box-shadow'>"
    + "<div class='post_content hidden fix-bottom-left box-pad-sm box-marg white bold'>{{content}}</div>"
    + "</div>"
  );

return Templates;
});
