define([
  'underscore',
  'backbone',
  'templates/Template'
], function(_, Backbone, Template) {
  var PostView = Backbone.View.extend({
 
  template: Template['post'],
  
  id: "post-container",
  className: "post-container",

  initialize: function () {
//    _.bindAll(this);
  },

  events: {
    'mouseover .post': 'showContent',
    'mouseout .post': 'hideContent'
  },

  render: function() {
    var posts = [{date: '10/29/2013', title: 'A new Post', content: 'A post created today..', imageSrc: 'http://www.thinkstockphotos.ca/CMS/StaticContent/WhyThinkstockImages/Creative.jpg'},
    {date: '10/29/2013', title: 'Another new Post', content: ' Another post created today..', imageSrc: 'http://www.thinkstockphotos.ca/CMS/StaticContent/WhyThinkstockImages/Creative.jpg'},
    {date:"", title:"this is a very happy day where me and you were super happies", content: "", imageSrc:"http://farm8.staticflickr.com/7371/12436283283_906a6c3a3b_n.jpg"},
    {date:"", title:"", content: "", imageSrc:"http://farm8.staticflickr.com/7371/12436283283_906a6c3a3b_n.jpg"},
    {date:"", title:"", content: "", imageSrc:"http://farm8.staticflickr.com/7371/12436283283_906a6c3a3b_n.jpg"},
    {date:"", title:"", content: "", imageSrc:"http://farm8.staticflickr.com/7371/12436283283_906a6c3a3b_n.jpg"},
    {date:"", title:"", content: "", imageSrc:"http://farm8.staticflickr.com/7371/12436283283_906a6c3a3b_n.jpg"},
    {date:"", title:"", content: "", imageSrc:"http://farm8.staticflickr.com/7371/12436283283_906a6c3a3b_n.jpg"},
    {date:"", title:"", content: "", imageSrc:"http://farm8.staticflickr.com/7371/12436283283_906a6c3a3b_n.jpg"},
    {date:"", title:"", content: "", imageSrc:"http://farm8.staticflickr.com/7371/12436283283_906a6c3a3b_n.jpg"},
    {date:"", title:"", content: "", imageSrc:"http://farm8.staticflickr.com/7371/12436283283_906a6c3a3b_n.jpg"},
    {date:"", title:"", content: "", imageSrc:"http://farm4.staticflickr.com/3803/12432729594_0e3f434c30.jpg"},
    {date:"", title:"", content: "", imageSrc:"http://i.telegraph.co.uk/multimedia/archive/02368/potd-cowboy-cat_2368962k.jpg"},
    {date:"", title:"", content: "", imageSrc:"http://farm4.staticflickr.com/3803/12432729594_0e3f434c30.jpg"},
    {date:"", title:"", content: "", imageSrc:"http://i.telegraph.co.uk/multimedia/archive/02368/potd-cowboy-cat_2368962k.jpg"},
    {date:"", title:"", content: "", imageSrc:"http://farm4.staticflickr.com/3803/12432729594_0e3f434c30.jpg"},
    {date:"", title:"", content: "", imageSrc:"http://i.telegraph.co.uk/multimedia/archive/02368/potd-cowboy-cat_2368962k.jpg"},
    {date:"", title:"", content: "", imageSrc:"http://farm4.staticflickr.com/3803/12432729594_0e3f434c30.jpg"},
    {date:"", title:"", content: "", imageSrc:"http://i.telegraph.co.uk/multimedia/archive/02368/potd-cowboy-cat_2368962k.jpg"},
    {date:"", title:"", content: "", imageSrc:"http://farm4.staticflickr.com/3803/12432729594_0e3f434c30.jpg"},
    {date:"", title:"", content: "", imageSrc:"http://i.telegraph.co.uk/multimedia/archive/02368/potd-cowboy-cat_2368962k.jpg"},
    {date:"", title:"", content: "", imageSrc:"http://farm4.staticflickr.com/3803/12432729594_0e3f434c30.jpg"},
    {date:"", title:"", content: "", imageSrc:"http://i.telegraph.co.uk/multimedia/archive/02368/potd-cowboy-cat_2368962k.jpg"},
    {date:"", title:"", content: "", imageSrc:"http://farm4.staticflickr.com/3803/12432729594_0e3f434c30.jpg"},
    {date:"", title:"", content: "", imageSrc:"http://i.telegraph.co.uk/multimedia/archive/02368/potd-cowboy-cat_2368962k.jpg"},
    {date:"", title:"", content: "", imageSrc:"http://farm4.staticflickr.com/3803/12432729594_0e3f434c30.jpg"},
    {date:"", title:"", content: "", imageSrc:"http://i.telegraph.co.uk/multimedia/archive/02368/potd-cowboy-cat_2368962k.jpg"},
    {date:"", title:"", content: "", imageSrc:"http://farm4.staticflickr.com/3803/12432729594_0e3f434c30.jpg"},
    {date:"", title:"", content: "", imageSrc:"http://i.telegraph.co.uk/multimedia/archive/02368/potd-cowboy-cat_2368962k.jpg"},
    {date:"", title:"", content: "", imageSrc:"http://images.nationalgeographic.com/wpf/media-live/photos/000/355/cache/best-pictures-night-sky-astrophotography-photo-contest-aurora-borealis-iceland_35561_600x450.jpg"}
    
    ];
    this.drawPost(posts);
    return this;
  },

  drawPost: function(posts) {
    for(var i = 0; i < posts.length; i++) {
      this.$el.append(this.template({
        date: posts[i].date,
        title: posts[i].title,
        content: posts[i].content,
        imageSrc: posts[i].imageSrc
      }));
    }
  },

  showContent: function(e) {
    $(e.currentTarget).children('.post_title').show();//.show('fade');
  },

  hideContent: function(e) {
    $(e.currentTarget).children('.post_title').hide();//.hide('fade');
  }

  });
  
  return PostView;

});
